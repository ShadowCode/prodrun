/*
Author: Veselin Nikolov
Terms of the MIT License:
--------------------------------------------------------------------

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef _PROC_LIST_H
#define _PROC_LIST_H

// System includes
#include <stdio.h>
#ifdef _WIN32
#include <Windows.h>
#endif

#define MAX_COMMAND_LENGTH 260

#ifdef _WIN32
typedef DWORD PID_t;
#else
typedef unsigned int PID_t;
#endif

typedef struct
{
	PID_t pid;
	char command[MAX_COMMAND_LENGTH + 1];
} process_t;

process_t* process_create(PID_t pid, const char* command);
int process_destroy(process_t** self);

typedef struct
{
	process_t** proc_list;
	unsigned short proc_list_size;
	char path_to_list[MAX_COMMAND_LENGTH + 1];
} proc_list_t;

proc_list_t* proc_list_create(const char* path_to_list);
int proc_list_destroy(proc_list_t** self);

int proc_list_check(proc_list_t* self);
int proc_list_enforce(process_t* self, proc_list_t* proc_list);

#endif // _PROC_LIST_H
