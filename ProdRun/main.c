/*
	Author: Veselin Nikolov
	Terms of the MIT License:
	--------------------------------------------------------------------

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
	including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
	and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
	IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
	TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// User includes
#include "proc_list.h"

#ifdef _WIN32
#define TakeABrake(_sec) Sleep(_sec*1000)
#else
#define TakeABrake(_sec) sleep(_sec)
#endif

#define DEFAULT_CHECK_INTERVAL_SEC 3
#define DEFAULT_PROC_LIST_PATH "proc.ls"


int G_IsRunning;

int free_and_close(DWORD CtrlType)
{
	G_IsRunning = FALSE;

	return 0;
}

int main(int argc, char** argv)
{
#ifdef _WIN32
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)free_and_close, TRUE) == FALSE)
	{
		printf("Unable to set control handler!");
		return 1;
	}
#endif
	// Read arguments
	char _path_to_list[MAX_COMMAND_LENGTH + 1];
	strncpy_s(_path_to_list, sizeof(_path_to_list), DEFAULT_PROC_LIST_PATH, sizeof(_path_to_list));
	if (argc == 2) {
#ifdef _WIN32
		strncpy_s(_path_to_list, sizeof(_path_to_list), argv[2], sizeof(_path_to_list));
#else
		strncpy(_path_to_list, argv[2], sizeof(_path_to_list));
#endif
	}

	// Set default state
	proc_list_t* _proc_list = proc_list_create(_path_to_list);
	G_IsRunning = TRUE;
	// Start running
	for (; G_IsRunning;)
	{
		proc_list_check(_proc_list);
		TakeABrake(DEFAULT_CHECK_INTERVAL_SEC);
	}
	// Teardown
	proc_list_destroy(&_proc_list);

	return 0;
}