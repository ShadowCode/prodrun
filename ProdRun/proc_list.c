/*
Author: Veselin Nikolov
Terms of the MIT License:
--------------------------------------------------------------------

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "proc_list.h"

// Forward declarations
int read_proc_list(proc_list_t* self);
int write_proc_list(proc_list_t* self);

int spawn_process(process_t* self);
int process_enforce(process_t* self);

///////////////////////////////////
// Process implementation
///////////////////////////////////
process_t* process_create(PID_t pid, const char* command) {
	process_t* self = (process_t*)malloc(sizeof(process_t));
	memset(self, 0, sizeof(process_t));

	self->pid = pid;
#ifdef _WIN32
	strncpy_s(self->command, sizeof(self->command), command, sizeof(self->command));
#else
	strncpy(self->command, command, sizeof(self->command));
#endif

	return self;
}

int process_destroy(process_t** self) {
	free(*self);
	*self = NULL;
	return 0;
}

int spawn_process(process_t* self) {
	if (self->command == NULL)
		return 1;

#ifdef _WIN32
	STARTUPINFOA startupInfo;

	memset(&startupInfo, 0, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);
#ifndef _DEBUG
	startupInfo.dwFlags = STARTF_USESHOWWINDOW;
	startupInfo.wShowWindow = SW_HIDE;
#endif

	PROCESS_INFORMATION processInformation;
	if (!CreateProcessA(NULL, self->command,
		NULL, NULL, FALSE, CREATE_NEW_CONSOLE,
		NULL, NULL, &startupInfo, &processInformation))
	{
		DWORD _err = GetLastError();
		printf("spawn_process [%s] Err: %d\n", self->command, _err);
		return _err;
	}
	CloseHandle(processInformation.hProcess);
	CloseHandle(processInformation.hThread);

	self->pid = processInformation.dwProcessId;
#else
	// POSIX implementation
#endif

	return 0;
}

///////////////////////////////////
// ProcList implementation
///////////////////////////////////
proc_list_t* proc_list_create(const char* path_to_list) {
	proc_list_t* self = (proc_list_t*)malloc(sizeof(proc_list_t));
	memset(self, 0, sizeof(proc_list_t));

#ifdef _WIN32
	strncpy_s(self->path_to_list, sizeof(self->path_to_list), path_to_list, sizeof(self->path_to_list));
#else
	strncpy(self->path_to_list, path_to_list, sizeof(self->path_to_list));
#endif
	// Read initial list
	read_proc_list(self);

	return self;
}

int proc_list_destroy(proc_list_t** self) {
	// Save changes to list
	write_proc_list(*self);

	if ((*self)->proc_list != NULL)
		free(&(*self)->proc_list);

	free(*self);
	*self = NULL;
	return 0;
}

int proc_list_check(proc_list_t* self) {

	// Update list
	int _err = read_proc_list(self);
	if (_err != 0) {
		printf("proc_list_check unable to sync list.\n");
		return _err;
	}

	for (unsigned int i = 0; i < self->proc_list_size; ++i)
	{
		if (self->proc_list[i] != NULL)
		{
			if (proc_list_enforce(self->proc_list[i], self) != 0) {
				printf("Process list item unable to enforce !!!\n");
			}
		}
	}
	return 0;
}

int proc_list_enforce(process_t* self, proc_list_t* proc_list) {
	// Check if process is alive
#ifdef _WIN32
	HANDLE hProcess = OpenProcess(SYNCHRONIZE, FALSE, self->pid);
	if (hProcess != NULL) {
		CloseHandle(hProcess);
	}
	else { // Need's resurection
		if (spawn_process(self) != 0) {
			printf("Unable to spawn process !!!\n");
			return 1;
		}
		else {
			// Update proc list
			write_proc_list(proc_list);
		}
	}
	
#else
	// POSIX implementation
#endif
	return 0;
}

int read_proc_list(proc_list_t* self) {

	self->proc_list_size = 0;
	// Open file holding the list of processes to care about
#ifdef _WIN32
	FILE* _fd;
	if (fopen_s(&_fd, self->path_to_list, "r+")) {
		printf("read_proc_list unable to read file.\n");
		return 1;
	}
#else
	FILE* _fd = fopen(self->path_to_list, "r+");
	if (_fd == NULL) {
		printf("read_proc_list unable to read file.\n");
		return 1;
	}
#endif
	// Count the number of lines in the file
	char _ch;
	while (!feof(_fd)) {
		_ch = fgetc(_fd);
		if (_ch == '\n') ++(self->proc_list_size);
	}
	fseek(_fd, 0L, SEEK_SET);

	if (self->proc_list != NULL) {
		for (unsigned short i = 0; i < self->proc_list_size; ++i) {
			if (self->proc_list[i] != NULL) {
				process_destroy(&self->proc_list[i]);
			}
		}
		free(self->proc_list);
	}

	self->proc_list = (process_t**)malloc(sizeof(process_t*) * self->proc_list_size);
	memset(self->proc_list, 0, sizeof(process_t*) * self->proc_list_size);
	
	// Read the file
	process_t* _temp_process = process_create(0, "");
	for (unsigned short i = 0; i < self->proc_list_size; ++i) {
#ifdef _WIN32
		fscanf_s(_fd, "%lu::%[^\r\n]", &(_temp_process->pid), &(_temp_process->command), (unsigned int)sizeof(_temp_process->command));
#else
		fscanf(_fd, "%lu::%[^\r\n]",  &(_temp_process->pid), &(_temp_process->command));
#endif
		// New line correction
		/*if (strlen(_temp_process->command) > 1) {
			_temp_process->command[strlen(_temp_process->command) - 1] = 0;
		}*/
		if (self->proc_list[i] == NULL) {
			self->proc_list[i] = process_create(_temp_process->pid, _temp_process->command);
		}
	}
	process_destroy(&_temp_process);

	// Close the file descriptor to the list of processes
	fclose(_fd);

	return 0;
}

int write_proc_list(proc_list_t* self) {
	// Open file holding the list of processes to care about
#ifdef _WIN32
	FILE* _fd;
	if (fopen_s(&_fd, self->path_to_list, "w+")) {
		printf("read_proc_list unable to write file.\n");
	}
#else
	FILE* _fd = fopen(self->path_to_list, "w+");
#endif

	for (unsigned short i = 0; i < self->proc_list_size; ++i) {
		if (self->proc_list[i] == NULL)
			continue;
		fprintf(_fd, "%lu::%s\r\n", (self->proc_list[i]->pid), &(self->proc_list[i]->command));
	}

	// Close the file descriptor to the list of processes
	fclose(_fd);

	return 0;
}
